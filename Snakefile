# /*
# * ----------------------------------------------------------------------------
# * Hyena Analysis (A snakemake file and its config.json) is written by Amin
# * Saffari under "THE BEER-WARE LICENSE" (Revision 42): 
# * <phk@FreeBSD.ORG> wrote this.  As long as you retain this notice you can 
# * do whatever you want with this stuff. If we meet some day, and you think 
# * this stuff is worth it, you can buy me a beer in return.
# * Poul-Henning Kamp
# * ----------------------------------------------------------------------------
# */

from snakemake.utils import R
import sys

def _individuals():
        Names = dict()
        for name,rest in config["INDIVIDUALS"].items():
          Names[name]=str(config["INDIVIDUALS"][name]["Download"])
        return Names

def _indiSexPop():
        IndiETC = list()
        for name,rest in config["INDIVIDUALS"].items():
          IndiETC.append(str(name)+"\t"+str(config["INDIVIDUALS"][name]["Sex"])+"\t"+str(name))
        return IndiETC

def _models():
        Names = dict()
        for name,rest in config["MODELS"].items():
            Names[name]=str(rest["kmax"])
        return Names

def _references():
        Names = dict()
        for name,rest in config["INDIVIDUALS"].items():
            if "Reference" in config["INDIVIDUALS"][name]:
                Names[name]=str(config["INDIVIDUALS"][name]["Reference"])
        return Names

def _fractions():
        Items = list()
        for foo,bar in config["FRACTIONS"].items():
             Items.append(str(foo))
        return Items

def _vcftypes():
        Items = list()
        for foo,bar in config["VCFTYPES"].items():
             Items.append(str(foo))
        return Items


##-----------------------------------------------##
## Define the Reference location and parameters  ##
##-----------------------------------------------##

WHERE            = config["WHERE"]
INDIVIDUALS      = _individuals()
REFERENCES       = _references() 
MODELS           = _models() 
FRACTIONS        = _fractions()
MIG              = range(0,5)
NSNP             = range(2000,5000,100)
BLENGTH          = range(200,300,100)
BLOCKLRNGTH      = config["blocklength"]
MININFO          = config["minInformative"]
VCFTYPES         = _vcftypes()

##-----------------------------------------------##
## Set software directories                      ##
##-----------------------------------------------##

HEFFALUMP= config["HEFFALUMP"]
TREEMIX  = config["TREEMIX"]
ABLE     = config["ABLE"]

##-----------------------------------------------##
## Cookbook Recipes                              ##
##-----------------------------------------------##

rule all:
    input:
       # treemix   = expand("{where}/REF_{by}/Results/Treemix/plot/All.{mig}.{type}.{nSNP}.pdf", where=WHERE,by=REFERENCES.keys(),mig=MIG,type=VCFTYPES,nSNP=NSNP),
       able      = expand("{where}/REF_{by}/results/ABLE/{model}.{type}.{blockLength}.{minInfo}.able", where=WHERE,by=REFERENCES.keys(), model= MODELS.keys(), type=VCFTYPES, blockLength=BLOCKLRNGTH, minInfo= MININFO)

rule getIt:
    input:
    params:
       address = lambda wildcards: unpack(INDIVIDUALS.get(wildcards.individual,"Bo")),
       Name = "getIt",
       Threads = 1
    threads: 1
    message: "downloading {wildcards.individual} of {wildcards.by} at {output.vcf}"
    output:
       vcf = "{where}/REF_{by}/{individual}/vcf/{individual}_{frac}.vcf.gz"
    shell:
       "wget --no-check-certificate {params.address} -O {output.vcf}"


rule heffit:
    input:
       vcf = "{where}/REF_{by}/{individual}/vcf/{individual}_{frac}.vcf.gz"
    params:
       twoBit = lambda wildcards,input : unpack(REFERENCES.get(wildcards.by,"Bo")),
       Name = "heffit",
       Threads = 1
    threads: 1
    output:
       heffa = "{where}/REF_{by}/{individual}/heffa/{individual}_{type}_{frac}.hef"
    message: "vcf {wildcards.individual} to hef {wildcards.by} ({params.twoBit})"
    shell: "{HEFFALUMP} vcfin -o {output.heffa} -r {params.twoBit} --{wildcards.type} -H {input.vcf}"

rule individuals:
 #   input:
 #      heffas = expand("{where}/REF_{{by}}/{individual}/heffa/{individual}_{type}_{frac}.hef",where=WHERE, individual=INDIVIDUALS,type=VCFTYPES,frac=FRACTIONS)
    params:
       INDISEXPOP  = _indiSexPop()
    message: "make the list of individuals to be in treemix"
    output:
       indSeparated = "{where}/REF_{by}/Results/Treemix/data/individuals.ind"
    run:
         with open(output.indSeparated, "w") as out:
            for ind in params.INDISEXPOP :
              out.write(ind+"\n")

rule mathematica:
    input:
       heffas = "{where}/REF_{by}/{individual}/heffa/{individual}_{type}_{frac}.hef"
    params:
       twoBit = lambda wildcards,input: unpack(REFERENCES.get(wildcards.by,"Bo")),
    message: "patch {wildcards.by} reference to make a fasta file"
    output:
       patch = "{where}/REF_{by}/{individual}/patch/{individual}_{type}_{frac}.fasta"
    shell: "{HEFFALUMP} patch -o {output.patch} -r {params.twoBit} -s {input.heffas}"

rule makeTreemix:
    input:
       individuals = "{where}/REF_{by}/Results/Treemix/data/individuals.ind",
       heffas = expand("{where}/REF_{{by}}/{individual}/heffa/{individual}_{{type}}_{frac}.hef",where=WHERE, individual=INDIVIDUALS,frac=FRACTIONS)
    params:
       twoBit = lambda wildcards,input: unpack(REFERENCES.get(wildcards.by,"Bo")),
    message: "make treemix inputs with {wildcards.by} as the reference"
    output:
       tmx = "{where}/REF_{by}/Results/Treemix/data/All.{type}.tmx.gz"
    shell: "{HEFFALUMP} treemix -n 0 -i {input.individuals} -o {output.tmx} -r {params.twoBit} {input.heffas}"

rule treemix:
    input:
       snp = "{where}/REF_{by}/Results/Treemix/data/All.{type}.tmx.gz"
    params:
       finishIt = lambda wildcards, output: output[0][:-5]
    message: "run treemix with {wildcards.by} as reference with number of migration = {wildcards.mig}"
    output:
       tmx  = "{where}/REF_{by}/Results/Treemix/data/All.{mig}.{type}.{nSNP}.llik"
    shell:
       "{TREEMIX}/src/treemix -bootstrap 100 -i {input.snp} -m {wildcards.mig} -k {wildcards.nSNP} -o {params.finishIt}"

rule drawTreemix:
    input:
       treemix = "{where}/REF_{by}/Results/Treemix/data/All.{mig}.{type}.{nSNP}.llik"
    params:
       prefix = lambda wildcards, input: input[0][:-5]
    message: "Draw treemix output {wildcards.by} as the reference"
    output:
       tmxPlot  = "{where}/REF_{by}/Results/Treemix/plot/All.{mig}.{type}.{nSNP}.pdf"
    run:
        
        R("""
        list.of.packages <- c(\"RColorBrewer\")
        new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,\"Package\"])]
        if(length(new.packages)) install.packages(new.packages,repos=\"http://cloud.r-project.org/\");
        source(\"{TREEMIX}/src/plotting_funcs.R\");
        pdf(file=\"{output.tmxPlot}\")
        plot_tree(\"{params.prefix}\",ybar = 0.1);
        dev.off()
        """)

rule makePseudoMS:
    input:
       heffas = expand("{{where}}/REF_{{by}}/{individual}/heffa/{individual}_{{type}}_{frac}.hef", individual=INDIVIDUALS.keys(),frac=FRACTIONS)
    params:
       twoBit = lambda wildcards,input: unpack(REFERENCES.get(wildcards.by,"Bo")),
       Name = "makePseudoMS",
       Threads = 1
    threads: 1
    message: "make Pseudo_MS with blockLength={wildcards.blockLength} and minInformative={wildcards.minInfo} on {wildcards.by} as the reference"
    output:
       able = "{where}/REF_{by}/bSFS/{type}.{blockLength}.{minInfo}.PseudoMS"
    shell: "{HEFFALUMP} pseudo_ms --min-informative {wildcards.minInfo} --blocklength {wildcards.blockLength} -r {params.twoBit} {input.heffas} > {output.able}"

rule convertConfig:
    input:
       pseudoMS = "{where}/REF_{by}/bSFS/{type}.{blockLength}.{minInfo}.PseudoMS"
    params:
       bsfsFile = lambda wildcards, input: input.pseudoMS[:-8] + "bSFS",
       Name = "convertConfig",
       Threads = 1
    threads: 1
    message: "make a config file for converting pseudoMS to bSFS for ABLE"
    output:
       configFile = "{where}/REF_{by}/configs/convert.{type}.{blockLength}.{minInfo}.Config"
    run:
         with open(output.configFile, "w") as out:
             out.write("# Converting input file to bSFS format\n")
             out.write("pops "+ config["CONVERSION"]["pops"] + "\n")
             out.write("datafile_format pseudo_MS\n")
             out.write("datafile "+ input.pseudoMS +"\n")
             out.write("convert_data_to_bSFS "+ params.bsfsFile +"\n")

rule bSFSConfig:
    input:
       configABLE = "{where}/REF_{by}/configs/convert.{type}.{blockLength}.{minInfo}.Config",
       pseudoMS = "{where}/REF_{by}/bSFS/{type}.{blockLength}.{minInfo}.PseudoMS"
    params:
       Name = "bSFSConfig",
       Threads = 1
    threads: 1
    message: "converting Pseudo_MS to its freq "
    output:
       able = "{where}/REF_{by}/bSFS/{type}.{blockLength}.{minInfo}.bSFS"
    shell: "{ABLE} -T {input.configABLE}"

rule modelConfig:
    input:
       bSFS  = "{where}/REF_{by}/bSFS/{type}.{blockLength}.{minInfo}.bSFS",
    params:
       bsfsFile = lambda wildcards, input: input.bSFS[:-8] + "bSFS",
       Name = "modelConfig",
       Threads = 1
    threads: 1
    message: "make a config file for model {wildcards.model}"
    output:
       configFile = "{where}/REF_{by}/configs/{model}_{type}.{blockLength}.{minInfo}.Config"
    run:
         with open(output.configFile, "w") as out:
             out.write("# Configfile for "+ wildcards.model +" of hyena project\n")
             out.write("datafile "+ input.bSFS +"\n")
             out.write("datafile_format bSFS\n")
             out.write("task infer\n")
             out.write("folded\n")
        
             for name,rest in config["MODELS"][wildcards.model].items():
                 if isinstance(rest,dict):
                     for name2,rest2 in config["MODELS"][wildcards.model][name].items():
                         out.write(name + " " +name2 + " " + rest2 + "\n")
                     out.write("\n")
                 else:
                     if name != "ABLE" :
                         out.write(name + " " + rest + "\n")
                     out.write("\n")

rule able:
    input:
       bsfs = "{where}/REF_{by}/bSFS/{type}.{blockLength}.{minInfo}.bSFS" ,
       configFile = "{where}/REF_{by}/configs/{model}_{type}.{blockLength}.{minInfo}.Config"
    params:
       ABLE = lambda wildcards: config["MODELS"][wildcards.model]["ABLE"].replace('LENGTH', str(int(wildcards.blockLength) + 1)),
       Name = lambda wildcards: wildcards.model,
       Threads = lambda wildcards: config["MODELS"][wildcards.model]["set_threads"]
    threads: 30
    message: "Fit model {wildcards.model} given {wildcards.type}.{wildcards.blockLength}.bSFS"
    output:
       able = "{where}/REF_{by}/results/ABLE/{model}.{type}.{blockLength}.{minInfo}.able"
    shell: ABLE + " {params.ABLE} -T {input.configFile} > {output.able}"

