# Hyena

Here is our Hyena analysis "pipeline". Everything in this project would be
under "THE BEER-WARE LICENSE" (Revision 42) unless we mention it.


## Dryrun:
To check if your workflow is defined properly:

`snakemake --dryrun --configfile config.json`

## Normal run:
`snakemake --configfile config.json`

## Parallel run:
If you have enough of CPUs (#CPUs modulo 30 = N) then replace the N with
the number of parallel jobs you want to run:

`snakemake --jobs N --configfile config.json`

## Submit to SGE:

If you don't have enough of cpus in your computer but want to be in a queue:

`snakemake --cluster "qsub -pe smp {params.Threads} -N {params.Name} " --jobs 10 --configfile config.json`

